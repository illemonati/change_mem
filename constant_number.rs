

fn main() {
    let number = 100000;
    loop {
        println!("number: {} | pointer location: {:p} | pid: {}", &number, &number, std::process::id());
        std::thread::sleep_ms(2000);
    }
}
