import mem_edit
from mem_edit import Process
import sys
import ctypes

pid = Process.get_pid_by_name('constant_number')
print('pid: {}'.format(pid))
with Process.open_process(pid) as p:
    assert isinstance(p, Process)
    addrs = p.search_all_memory(ctypes.c_int32(100000))
    assert(len(addrs) == 1)
    addr = addrs[0]
    print(f'addr: {addr}')
    number = p.read_memory(addr, ctypes.c_int32())
    print(f'number: {number}')
    try:
        p.write_memory(addr, ctypes.c_int32(int(sys.argv[1])))
    except Exception as e:
        print("pass the new number in as the 1st argument")
    number = p.read_memory(addr, ctypes.c_int32())
    print(f'new number: {number}')
